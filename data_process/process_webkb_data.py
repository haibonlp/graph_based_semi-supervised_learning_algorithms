# coding=utf-8
import sys
import random
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import pairwise_kernels
from sklearn.neighbors import NearestNeighbors
from scipy.io import mmwrite, mmread
from scipy.sparse import lil_matrix

class WebkbProcessor:
  def __init__(self):
    self.docId2text = {}
    self.tag2docIds = {}
    self.vectorizer = TfidfVectorizer(min_df=1)

  def load_data(self, infile):
    errline =0
    In = open(infile)
    docId = 0
    for line in In:
      line = line.strip()
      if len(line) <=0:
        continue
      termlist = line.split('\t')
      if len(termlist) !=2 :
        #print 'Error '
        errline += 1
        continue
        #raise ValueError("Format error! text: %s"%line)
      tag = termlist[0].strip()
      text = termlist[1].strip()
      #text = [ t.lower().strip() for t in text ]
      self.docId2text[docId] = text
      if tag not in self.tag2docIds:
        self.tag2docIds[tag] = [docId]
      else:
        self.tag2docIds[tag].append(docId)
      docId += 1
    In.close()

    print 'error line : ', errline
    print ' loading complete : '
    for tag in self.tag2docIds:
      print '\t\t ', tag, '  num : ', len(self.tag2docIds[tag])

    print self.tag2docIds[tag][0]


  def split_data(self):
    self.train_data = {}
    self.test_data = {}
    train_size = 0
    test_size = 0
    for tag in self.tag2docIds:
      texts = self.tag2docIds[tag]
      random.shuffle(texts)
      trainSize = len(texts)*1/4
      self.train_data[tag] = texts[:trainSize]
      self.test_data[tag] = texts[trainSize:]

      train_size += len(self.train_data[tag])
      test_size += len(self.test_data[tag])

      print 'Tag : ', tag
      print '     train :', len(self.train_data[tag])
      print '     test  :', len(self.test_data[tag])

    print 'all_train_size : ', train_size
    print 'all_test_size  : ', test_size


  def get_labeled_data(self):
    Nl = 48
    traindata = []
    for tag in self.train_data:
      for docId in self.train_data[tag]:
        instance = (tag, docId)
        traindata.append(instance)
    labeled_data = []
    getData = False
    while( not getData ):
      labeled_data = []
      random.shuffle(traindata)
      tag2inst = {}
      labeled_data = traindata[:Nl]
      for inst in labeled_data:
        tag = inst[0]
        if tag not in tag2inst:
          tag2inst[tag] = 1
        else:
          tag2inst[tag] += 1
      if len(tag2inst.keys()) == 4:
        getData = True

    print 'labeled_data : ', tag2inst

    return labeled_data


  def get_unlabeled_data(self):
    data = []
    for tag in self.test_data:
      for docId in self.test_data[tag]:
        inst = (tag, docId)
        data.append(inst)
    return data




  def create_graph(self):
    labeled_data = self.get_labeled_data()
    unlabeled_data = self.get_unlabeled_data()
    data = labeled_data + unlabeled_data
    print ' graph data size : ', len(data)
    texts = []
    tag2id = {}
    for inst in data:
      texts.append( self.docId2text[inst[1]] )
      tag = inst[0].strip()
      if tag not in tag2id:
        tag2id[tag] = len(tag2id)

    #print texts[0]
    X = self.vectorizer.fit_transform(texts)
    #print X
    #K = pairwise_kernels(X, metric="cosine")
    #print K
    nbrs = NearestNeighbors(n_neighbors=self.num_nbrs,algorithm='brute', metric='cosine')
    nbrs.fit(X)
    distances, indices = nbrs.kneighbors(X, n_neighbors=self.num_nbrs)
    similarities = 1.0 - distances

    print 'distances    : \n', distances
    print 'similarities : \n', similarities
    print 'indices : \n', indices

    num_inst = len(data)

    W = lil_matrix((num_inst, num_inst))
    for i in xrange(num_inst):
      for j in xrange(self.num_nbrs):
        sim = similarities[i,j]
        nbr = indices[i, j]
        W[i, nbr] = sim
        W[nbr, i] = sim

    mmwfile = self.outprefix + '-weight.mtx'
    mmwrite(mmwfile, W)
    #newW = mmread(mmwfile)
    #print 'newW : ', type(newW)

    self.write_tag_file(data, tag2id)

    num_labeled = len(labeled_data)
    self.get_init_Y( data, tag2id, num_labeled)


  def get_init_Y(self, data, tag2id, num_labeled):
    num_tag = len(tag2id)
    num_inst = len(data)
    initY = lil_matrix((num_inst, num_tag))
    for i, inst in enumerate( data[:num_labeled] ):
      tid = tag2id[inst[0]]
      initY[i, tid] = 1.0

    initfile = self.outprefix + '-initlabel'
    mmwrite(initfile, initY)
    #Y = mmread(initfile)
    #Y = Y.todense()
    #print '---- initY : \n', Y[:num_labeled+5, :]


  def write_tag_file(self, data, tag2id ):
    tagfile = self.outprefix + '-label.tag'
    Out = open(tagfile, 'w')
    for inst in data:
      tag = inst[0]
      tid = tag2id[tag]
      Out.write( tag + '\t'+ str(tid) + '\n')
    Out.close()










  def process(self, infile):
    self.load_data(infile)
    self.split_data()
    self.create_graph()



def main():
  infile = '/uusoc/scratch/res/nlp/haibo/my-research/data/webkb-data/webkb-all-data'
  outprefix = '/uusoc/scratch/res/nlp/haibo/my-research/data/webkb-data/webkb-graph/sample-1'

  processor = WebkbProcessor()
  processor.outprefix = outprefix
  processor.num_nbrs = 5
  processor.process(infile)





if __name__ == '__main__':
  main()



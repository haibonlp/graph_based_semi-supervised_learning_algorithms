# coding=utf-8

import numpy as np
from scipy.sparse import csr_matrix
from sklearn.preprocessing import normalize

class BipartSpreader(object):
  def __init__(self, max_iter=100, tol=1e-3, debug=0):
    self.max_iter = max_iter
    self.tol = tol
    self.debug = debug


  def _is_converged(self, Y, Y_hat):
    loss = np.sum( np.abs( Y - Y_hat ) )
    if self.debug > 0:
      print 'loss :', loss
    return loss < self.tol



  def fit(self, bigraph):
    size_e = bigraph.size_e
    size_c = bigraph.size_c
    n_classes = bigraph.n_classes
    clam_weights = bigraph.alpha_vec

    Y0 = bigraph.init_labels
    Ye = np.copy(Y0)

    Wec = bigraph.weight_ec
    Wce = bigraph.weight_ce

    Ye_static = np.multiply(clam_weights, Y0)

    prop_weight = 1.0 - clam_weights

    Ye_pre = np.zeros(Y0.shape)
    YC = np.zeros((size_c, n_classes))

    num_iter = 0

    while( num_iter < self.max_iter ):
      if self._is_converged(Ye_pre, Ye):
        print 'Program converged at iter_num : ', num_iter
        break
      Ye_pre = Ye
      YC = Wce.dot(Ye)
      Ye = Wec.dot(YC)
      Ye = np.multiply( prop_weight, Ye) + Ye_static

      num_iter += 1

      #normalize(YC, norm='l1', axis=1, copy=False)
      #print ' YC : \n', YC

    normalize(YC, norm='l1', axis=1, copy=False)
    print ' YC : \n', YC


    normalize(Ye, norm='l1', axis=1, copy=False)

    return Ye











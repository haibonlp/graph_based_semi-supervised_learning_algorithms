#coding=utf-8

import numpy as np
from scipy.sparse import csr_matrix
from sklearn.preprocessing import normalize
from scipy import sparse as sp

'''
add Right-Normlize   W^et and W^te  :

'''

class SquareLossEventTopicMPHeterCocTTv4():
  def __init__(self, max_iter=10, alpha=1.0, beta=1.0, gamma=1.0, rho=1.0,
      tau=1.0, xi=1.0, eta=1.0, sigma=1.0, tol=1e-2, debug=0):
    self.max_iter = max_iter
    self.alpha = alpha ## event-event part weight
    self.beta  = beta  ## event prior weight
    self.gamma = gamma ## event-to-topic part weight
    self.rho = rho  ## dis graph weight
    self.tau = tau  ## co-occur graph weight
    self.xi = xi   ## T-T sim graph weight
    self.eta = eta  ## topic local weight
    self.sigma = sigma ## topic-to-event part weight
    self.tol = tol
    self.debug = debug

  def _is_converged(self, Y, Y_hat):
    loss = np.sum( np.abs( Y - Y_hat ) )
    if self.debug:
      print '  difference : ' , loss
    return loss < self.tol


  def get_norm_matrix(self, W):
    D1 = W.sum(axis=0).A[0]
    D1[D1==0] += 1  # Avoid division by 0
    D2 = np.sqrt(sp.diags((1.0/D1),offsets=0))
    W = D2.dot(W).dot(D2)

    return W

  def left_norm_matrix(self, W):
    D1 = W.sum(axis=1).A[:, 0]
    D1[D1==0] += 1  # Avoid division by 0
    D2 = sp.diags((1.0/D1),offsets=0)
    #D2 = np.sqrt(sp.diags((1.0/D1),offsets=0))
    W = D2.dot(W)

    return W

  def right_norm_matrix(self, W):
    ## get colum sum
    d2 = W.sum(axis=0).A[0]
    d2[d2==0] += 1
    d2 = sp.diags((1.0/d2),offsets=0)
    W = W.dot(d2)
    return W




  def normalize_asym_matrix(self, W):
    d1 = W.sum(axis=1).A[:, 0]
    d1[d1==0] += 1
    d1 = np.sqrt(sp.diags((1.0/d1),offsets=0))
    d2 = W.sum(axis=0).A[0]
    d2[d2==0] += 1
    d2 = np.sqrt(sp.diags((1.0/d2),offsets=0))
    W = d1.dot(W).dot(d2)
    return W




  def fit(self, graph):
    n_instances = graph.n_instances
    n_classes   = graph.n_classes

    Y_init = graph.init_labels
    Y_init = np.array(Y_init)

    Y = np.copy(Y_init)

    W = graph.weight_matrix
    Wdis = graph.dissimilar_weight_matrix

    Wcoc = graph.cooccur_weight_matrix

    Ts = graph.topic_seed_identity
    T_seed = graph.topic_seed_labels
    Wtt = graph.tt_sim_weight_matrix
    T_local = graph.topic_local_prior

    #T = np.copy(T_local)
    T = np.zeros( T_local.shape)


    W = self.get_norm_matrix(W)
    Wdis = self.get_norm_matrix(Wdis)
    Wcoc = self.get_norm_matrix( Wcoc )


    labeled = graph.get_labeled()

    Hsim = graph.Hsim
    Hdis = graph.Hdis

    # seed Identity
    S = np.zeros((n_instances, 1))
    S[labeled] = 1.0

    Z = graph.local_evidence

    ''' left normalize
    Wet = graph.event_topic_matrix
    Wte = Wet.transpose()
    Wet = self.left_norm_matrix( Wet )
    Wte = self.left_norm_matrix( Wte )
    '''

    Wet = graph.event_topic_matrix
    Wte = Wet.transpose()
    Wet = self.right_norm_matrix( Wet )
    Wte = self.right_norm_matrix( Wte )

    '''
    Wet = graph.event_topic_matrix
    Wet = self.normalize_asym_matrix( Wet )
    Wte = Wet.transpose()
    '''




    Y_pre = np.zeros((n_instances, n_classes))

    Y_denom = S + self.alpha * np.array( W.sum(axis=1) )
    Y_denom += self.rho * np.array( Wdis.sum(axis=1) )
    Y_denom += self.tau * np.array( Wcoc.sum(axis=1) )
    Y_denom += self.beta
    #print ' U : ', U.shape,  '  Usum : ', U.sum(axis=1).shape
    Y_denom += self.gamma * np.array( Wet.sum(axis=1)  )

    print ' Y_denom : ', Y_denom.shape


    T_denom = self.xi*Ts + self.xi * np.array( Wtt.sum(axis=1) )
    T_denom += self.eta
    T_denom += self.sigma * np.array( Wte.sum(axis=1) )
    print ' T_denom : ', T_denom.shape

    #T_pre = np.copy( T_local )
    T_pre = np.zeros( T_local.shape)


    num_iter = 0
    while( num_iter < self.max_iter):
      print 'iter : ', num_iter
      if num_iter >0 and  self._is_converged(Y_pre, Y):
        print 'TransductionWithConfidence converged at iter_num : ', num_iter
        break
      Y_pre = Y

      Y = S*Y_init + self.alpha* W.dot( Y_pre.dot(Hsim) )
      Y += self.rho * Wdis.dot( Y_pre.dot(Hdis) )
      Y += self.tau * Wcoc.dot( Y_pre.dot(Hsim) )

      Y += self.beta * Z
      Y += self.gamma * Wet.dot( T )
      Y /= Y_denom

      T_pre = T
      T  = self.xi* Ts*T_seed + self.xi * Wtt.dot( T_pre )
      T += self.eta * T_local
      T += self.sigma * Wte.dot( Y )
      T /= T_denom
      #normalize(T, norm='l1', axis=1, copy=False)
      print ' Ti vanquish: 11714 : ', T[11714]
      print ' Ti kill: 416 : ', T[416]

      num_iter += 1

    normalize(Y, norm='l1', axis=1, copy=False)

    return Y






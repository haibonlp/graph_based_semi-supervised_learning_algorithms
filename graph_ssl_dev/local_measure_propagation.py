#coding=utf-8

import numpy as np
from scipy import sparse
from sklearn.preprocessing import normalize
from sklearn.utils.graph import graph_laplacian

class LocalMeasurePropagation(object):
  def __init__(self, max_iter=100, alpha=1.0, mu=1.0, nu=1.0, tol=1e-3, debug=0 ):
    self.max_iter = max_iter
    self.tol = tol
    self.alpha = alpha
    self.mu = mu
    self.nu = nu

    self.debug = debug




  def _is_converged(self, Y, Y_hat):
    loss = np.sum( np.abs( Y - Y_hat ) )
    #print ' Y : \n', Y
    #print ' Y_hat : \n', Y_hat
    if self.debug:
      print '   loss : ', loss
    return loss < self.tol



  def fit(self, graph):
    n_instances = graph.n_instances
    n_classes   = graph.n_classes

    Y_init = graph.init_labels

    # W must be csr_matrix matrix
    W = graph.weight_matrix

    labeled = graph.get_labeled()
    r = np.copy(Y_init)
    # seed Identity
    #S = np.zeros((n_instances, 1))
    #S[labeled] = 1.0
    S = np.ones((n_instances, 1))

    W_hat = W + self.alpha * sparse.identity(n_instances, format='csr')
    W_hat_T = W_hat.transpose()
    gamma = self.nu + self.mu * W_hat.sum(axis=1)
    mu_gamma = self.mu/gamma
    q_denom = S + self.mu * ( W_hat.sum(axis=0).transpose() )

    ##print '\n  W_hat : \n', W_hat.todense()
    #print '\n  gamma : \n', gamma
    #print '\nmu_gamma : \n', mu_gamma
    #print '\n q_denom : \n', q_denom



    p_pre = np.zeros((n_instances, n_classes))
    p = np.copy(Y_init)

    q = r*S + self.mu* W_hat_T.dot(p)
    #print '\n q : \n', q
    q += 1e-10
    q /= q_denom
    #print '\n q / denom : \n', q

    num_iter = 0
    while( num_iter < self.max_iter ):
      if self._is_converged(p_pre, p):
        print 'Measure Propagation converged at iter_num : ', num_iter
        break
      print '##'*2, '   iter : ', num_iter
      p_pre = p
      log_q = np.log(q)
      #log_q[ log_q == float("-inf") ] = 0.
      #print '  log_q : \n', log_q
      #W_dot_q =  W_hat.dot( np.log(q))
      W_dot_q =  W_hat.dot( log_q)

      #print ' W_hat.dot(log(q)) :  \n', W_dot_q
      #print ' mu_gamma * W_dot_q : \n', np.multiply(mu_gamma, W_dot_q)
      p = np.exp( np.multiply( mu_gamma,  W_dot_q  ) )
      #print '\n ---  p :  \n', p
      normalize(p, norm='l1', axis=1, copy=False)
      #print '\n ---  norm p :  \n', p

      q = r*S + self.mu* W_hat_T.dot(p)
      #print ' \n --- q qq : \n', q
      q /= q_denom
      #print ' \n --- q/denom : \n', q

      num_iter += 1

    #print '\n ---  norm p :  \n', p
    return p













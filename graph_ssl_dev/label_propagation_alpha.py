# coding=utf-8

import numpy as np
from scipy.sparse import csr_matrix
from sklearn.preprocessing import normalize
from sklearn.utils.graph import graph_laplacian
#from sklearn.utils.extmath import safe_sparse_dot


class LabelPropagationAlpha(object):
  def __init__(self, max_iter=100, tol=1e-2, debug=0):
    self.max_iter = max_iter
    self.tol = tol
    self.debug = debug


  def _is_converged(self, Y, Y_hat):
    loss = np.sum( np.abs( Y - Y_hat ) )
    if self.debug > 0:
      print 'loss :', loss
    return loss < self.tol


  def _energy(self, Y, laplacian):
    energy_value = Y.transpose().dot( laplacian.dot(Y)   )
    energy_value = np.trace(energy_value)
    #energy_value = energy_value.sum()
    #energy_value = np.abs(energy_value).sum()
    #print 'energy_value : ', energy_value
    return energy_value



  def fit(self, graph):
    n_instances = graph.n_instances
    n_classes   = graph.n_classes
    clam_weights = graph.alpha_vec

    Y_init = graph.init_labels
    Y = np.copy(Y_init)

    # W must be csr_matrix matrix
    W = graph.weight_matrix
    laplacian = graph_laplacian(W)

    # always keep the original labels with factor alpha
    #Y_init *= self.alpha
    Y_static = np.multiply(clam_weights, Y_init)

    # only allow the labeled data to change with factor (1-alpha)
    #labeled = graph.get_labeled()
    #clam_weights = np.ones( (n_instances, 1) )
    #if clam_weights.shape != labeled.shape:
    #  raise ValueError("Shape not same for [clam_weights=%s] and [labeled=%s]" % ( clam_weights.shape, labeled.shape ))
    #clam_weights[ labeled ] = 1 - self.alpha
    prop_weight = 1.0 - clam_weights


    #print ' energy_value :  ', self._energy(Y, laplacian)

    #W /= W.sum(axis=1)
    normalize(W, norm='l1', axis=1, copy=False)
    #print ' normalized  weight_matrix : \n',  W

    Y_pre = np.zeros( Y_init.shape )
    num_iter = 0
    pre_energy = float('inf')
    while( num_iter < self.max_iter ):
      cur_energy = self._energy(Y, laplacian)
      if self.debug > 0:
        print 'iter : ', num_iter,
        print '  energy_value : ', cur_energy
      #if self._is_converged(Y_pre, Y):
      #  print 'Program converged at iter_num : ', num_iter
      #  break


      diff_energy = abs(cur_energy - pre_energy)
      if diff_energy < self.tol:
        print 'Program converged at iter_num : ', num_iter
        break
      pre_energy = cur_energy


      #print '##'*20, '\n\n   iter : ', num_iter
      Y_pre = Y
      #print '\b Y : ', Y
      Y = W.dot( Y )
      #print 'dot : \n', Y
      #Y = np.multiply( clam_weights, Y ) + Y_init
      Y = np.multiply( prop_weight, Y ) + Y_static
      #print '\nclamp : \n', Y

      num_iter += 1

      #print ' energy_value :  ', self._energy(Y, laplacian)

    print 'run ', num_iter, ' iterations !'
    ##Y_normalizer = np.sum(Y, axis=1).reshape( n_instances, 1)
    ##print Y_normalizer
    ##Y /= Y_normalizer
    normalize(Y, norm='l1', axis=1, copy=False)


    if self.debug > 1:
      print ' normalized Y : \n', Y

    return Y







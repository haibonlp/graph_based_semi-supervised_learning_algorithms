#coding=utf-8

import numpy as np
from scipy.sparse import csr_matrix
from sklearn.preprocessing import normalize
from sklearn.utils.graph import graph_laplacian
from scipy import sparse

class LocalNormTransductionWithConfidence(object):
  def __init__(self, debug=0, max_iter=100, alpha=1.0, beta=1.0, gamma=1.0, tol=1e-3 ):
    self.max_iter = max_iter
    self.alpha = alpha
    self.beta = beta
    self.gamma = gamma
    self.tol = tol
    self.debug = debug

  def _is_converged(self, Y, Y_hat):
    loss = np.sum( np.abs( Y - Y_hat ) )
    if self.debug:
      print '  difference : ' , loss
    return loss < self.tol

  def _cost(self, Y, Y0, S, W, Sigma):
    ## to do
    #v1 = 1.0/2 *





    #cost = v1


    return 0


  def get_norm_matrix(self, W):
    '''
    L = D - W
    nL = D^{-1/2} * L * D^{-1/2} = I - D^{-1/2}* W *D^{-1/2}
    New_W = D^{-1/2} * W * D^{-1/2} = I - nL
    '''
    # normalized laplacian
    nL = graph_laplacian(W, normed=True)
    W = sparse.identity( W.shape[0]) - nL
    return W



  def fit(self, graph):
    n_instances = graph.n_instances
    n_classes   = graph.n_classes

    Y_init = graph.init_labels
    Y_init = np.array(Y_init)
    #print 'Y_init : ', type(Y_init), Y_init.shape
    Y = np.copy(Y_init)
    #Y = np.zeros((n_instances, n_classes)) ## set start value to be zero (0)

    # W must be csr_matrix matrix
    W = graph.weight_matrix

    W = self.get_norm_matrix(W)


    labeled = graph.get_labeled()

    # seed Identity
    #S = np.zeros((n_instances, 1))
    S = np.ones((n_instances, 1))
    #S[labeled] = 1.0

    Sigma = np.ones((n_instances, n_classes))

    W = W.tolil()

    Y_pre = np.zeros((n_instances, n_classes))

    num_iter = 0
    while( num_iter < self.max_iter):
      if num_iter >0 and  self._is_converged(Y_pre, Y):
        print 'TransductionWithConfidence converged at iter_num : ', num_iter
        break
      Y_pre = Y
      Sigma_pre = Sigma

      cur_cost = self._cost(Y, Y_init, S, W, Sigma )
      print '##'*2, '   iter : ', num_iter
      #print ' cur_cost : ', cur_cost

      #######################################
      ########  compute new Y and Sigma

      Y = np.zeros((n_instances, n_classes))
      Y_denom = np.zeros((n_instances, n_classes))
      Sigma = np.zeros((n_instances, n_classes))

      for i, column in enumerate( W.rows ):
        sigma_inv_i =  1.0/Sigma_pre[i]
        P_i = S[i,0] * (sigma_inv_i + 1.0/self.gamma)
        #print 'P_i : ', type(P_i), P_i.shape, P_i
        #print 'Y_init[i] : ', type(Y_init[i, :]) , Y_init[i, :].shape , Y_init[i, :]
        Y[i] = P_i * Y_init[i]
        #print 'Y[i] : ', type(Y[i]) ,  Y[i]
        Y_denom[i] = P_i

        sigma_ti = S[i,0]* np.power( Y_pre[i]-Y_init[i], 2)

        #print '\n ----- row num : ', i
        #print '      P_i  : ', P_i
        #print '      Y[i--]--- : ', Y[i]
        #print '      Y_denom[i--] : ', Y_denom[i]
        #print '      sigma_ti   : ', sigma_ti
        #print '   ----------#######'

        for j in column:
          if i == j: continue
          #w_ij = W[i,j]
          K_ij = W[i,j] *( sigma_inv_i + 1.0/Sigma_pre[j] )
          Y[i] +=  K_ij * Y_pre[j]
          Y_denom[i] += K_ij

          #print '           K_ij : \t', K_ij, ' (i,j)=',i,j
          #print '     Y[%d, %d]  : \t'%(i, j), K_ij*Y_pre[j]


          sigma_ti += W[i,j]* np.power( Y_pre[i]-Y_pre[j], 2)


        Sigma[i] = (1.0/(2*self.alpha))*(self.beta + np.sqrt( self.beta*self.beta + 2.0*self.alpha * sigma_ti  ) )

        #if num_iter >= 2:
        #print ' ----  Sigma[i] : ', Sigma[i]
        #print '      Y[%d]--- : ' %i,  Y[i]
        #print '      Y_denom[%d] : '%i,  Y_denom[i]

      Y /= Y_denom
      #print ' -- Y after denom : \n', Y

      num_iter += 1

      #print '  new #### Y : \n', Y
      #print '  ###### new sigma  : \n', Sigma

    return Y












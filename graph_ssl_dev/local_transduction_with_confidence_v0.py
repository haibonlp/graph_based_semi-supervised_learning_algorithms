#coding=utf-8

import numpy as np
from scipy.sparse import csr_matrix
from sklearn.preprocessing import normalize
from sklearn.utils.graph import graph_laplacian

class LocalTransductionWithConfidenceV0(object):
  def __init__(self, debug=0, max_iter=100, alpha=1.0, beta=1.0, gamma=1.0, tol=1e-3 ):
    self.max_iter = max_iter
    self.alpha = alpha
    self.beta = beta
    self.gamma = gamma
    self.tol = tol
    self.debug = debug

  def _is_converged(self, Y, Y_hat):
    loss = np.sum( np.abs( Y - Y_hat ) )
    return loss < self.tol

  def _cost(self, Y, Y0, S, W, Sigma):
    ## to do
    #v1 = 1.0/2 *





    #cost = v1


    return 0



  def fit(self, graph):
    n_instances = graph.n_instances
    n_classes   = graph.n_classes

    Y_init = graph.init_labels
    Y_init = np.array(Y_init)
    #print 'Y_init : ', type(Y_init), Y_init.shape
    Y = np.copy(Y_init)
    #Y = np.zeros((n_instances, n_classes)) ## set start value to be zero (0)

    # W must be csr_matrix matrix
    W = graph.weight_matrix

    labeled = graph.get_labeled()

    # seed Identity
    #S = np.zeros((n_instances, 1))
    S = np.ones((n_instances, 1))
    #S[labeled] = 1.0

    Sigma = np.ones((n_instances, n_classes))

    W = W.tolil()

    Y_pre = np.zeros((n_instances, n_classes))

    num_iter = 0
    while( num_iter < self.max_iter):
      if num_iter >0 and  self._is_converged(Y_pre, Y):
        print 'TransductionWithConfidence converged at iter_num : ', num_iter
        break
      Y_pre = Y
      Sigma_pre = Sigma

      cur_cost = self._cost(Y, Y_init, S, W, Sigma )
      print '##'*2, '   iter : ', num_iter
      #print ' cur_cost : ', cur_cost

      #######################################
      ########  compute new Y and Sigma

      Y = np.zeros((n_instances, n_classes))
      Y_denom = np.zeros((n_instances, n_classes))
      Sigma = np.zeros((n_instances, n_classes))

      for i, column in enumerate( W.rows ):
        for c in range(n_classes):
          Y[i,c] +=  S[i,0]*( 1.0/Sigma_pre[i,c] + 1.0/self.gamma ) * Y_init[i,c]
          Y_denom[i,c] = S[i,0]*(1.0/Sigma_pre[i,c] + 1.0/self.gamma)

          sigma_ic = S[i,0]*np.power( Y_pre[i,c] - Y_init[i,c], 2.0)

          for j in column:
            if i==j :
              continue
            weight = W[i,j]*(1.0/Sigma_pre[i,c] + 1.0/Sigma_pre[j,c] )
            Y[i,c] += weight * Y_pre[j,c]
            Y_denom[i,c] += weight

            sigma_ic += W[i,j] * np.power(Y_pre[i,c] - Y_pre[j,c], 2.0 )

          Y[i,c] = Y[i,c]/Y_denom[i,c]

          Sigma[i,c] = ( 1.0/(2.0*self.alpha)) * ( self.beta + np.sqrt( self.beta*self.beta + 2*self.alpha * sigma_ic  )   )

      num_iter += 1

    return Y













#coding=utf-8

import numpy as np
from scipy.sparse import csr_matrix
from sklearn.preprocessing import normalize
from scipy import sparse as sp


class SquareLossEventTopicMPHeter():
  def __init__(self, max_iter=10, alpha=1.0, beta=1.0, gamma=1.0, rho=1.0, tol=1e-2, debug=0):
    self.max_iter = max_iter
    self.alpha = alpha
    self.beta  = beta
    self.gamma = gamma
    self.rho = rho  ## dis graph weight
    self.tol = tol
    self.debug = debug

  def _is_converged(self, Y, Y_hat):
    loss = np.sum( np.abs( Y - Y_hat ) )
    if self.debug:
      print '  difference : ' , loss
    return loss < self.tol


  def get_norm_matrix(self, W):
    D1 = W.sum(axis=0).A[0]
    D1[D1==0] += 1  # Avoid division by 0
    D2 = np.sqrt(sp.diags((1.0/D1),offsets=0))
    W = D2.dot(W).dot(D2)

    return W

  def left_norm_matrix(self, W):
    D1 = W.sum(axis=1).A[:, 0]
    D1[D1==0] += 1  # Avoid division by 0
    D2 = sp.diags((1.0/D1),offsets=0)
    #D2 = np.sqrt(sp.diags((1.0/D1),offsets=0))
    W = D2.dot(W)

    return W


  def fit(self, graph):
    n_instances = graph.n_instances
    n_classes   = graph.n_classes

    Y_init = graph.init_labels
    Y_init = np.array(Y_init)

    Y = np.copy(Y_init)

    W = graph.weight_matrix
    Wdis = graph.dissimilar_weight_matrix

    W = self.get_norm_matrix(W)
    Wdis = self.get_norm_matrix(Wdis)


    labeled = graph.get_labeled()

    Hsim = graph.Hsim
    Hdis = graph.Hdis

    # seed Identity
    S = np.zeros((n_instances, 1))
    S[labeled] = 1.0

    Z = graph.local_evidence
    T = graph.global_evidence
    U = graph.global_matrix
    U = self.left_norm_matrix(U)

    Y_pre = np.zeros((n_instances, n_classes))

    Y_denom = S + self.alpha * np.array( W.sum(axis=1) )
    Y_denom += self.rho * np.array( Wdis.sum(axis=1) )
    Y_denom += self.beta
    #print ' U : ', U.shape,  '  Usum : ', U.sum(axis=1).shape
    Y_denom += self.gamma * np.array( U.sum(axis=1)  )

    print ' Y_denom : ', Y_denom.shape


    num_iter = 0
    while( num_iter < self.max_iter):
      print 'iter : ', num_iter
      if num_iter >0 and  self._is_converged(Y_pre, Y):
        print 'TransductionWithConfidence converged at iter_num : ', num_iter
        break
      Y_pre = Y

      Y = S*Y_init + self.alpha* W.dot( Y_pre.dot(Hsim) )
      Y += self.rho * Wdis.dot( Y_pre.dot(Hdis) )

      Y += self.beta * Z
      Y += self.gamma * U.dot( T )
      Y /= Y_denom

      num_iter += 1

    normalize(Y, norm='l1', axis=1, copy=False)

    return Y






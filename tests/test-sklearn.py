
from sklearn.semi_supervised import LabelPropagation
import numpy as np
from scipy.sparse import csr_matrix
from scipy import sparse



def test_data2():
  X = np.array( [[1., 2.],
                 [3., 4.]] )

  #X = np.array( [[1., 2.],
  #               [2., 1.]] )


  print ' X : \n', X

  affinity_matrix = X
  #affinity_matrix = csr_matrix(affinity_matrix)
  degree_matrix = map(sum, affinity_matrix) * np.identity(affinity_matrix.shape[0])

  print 'degree_matrix: \n', degree_matrix
  deg_inv = np.linalg.inv(degree_matrix)
  print 'inv : \n', deg_inv

  aff_ideg = deg_inv * np.matrix(affinity_matrix)

  print 'aff_ideg : \n', aff_ideg, '\n', '##'*20






def test_data():
  X = np.array( [[1., 2.], [3., 4.]] )
  #X = np.array( [[1., 2.], [2., 1.]] )
  #X = np.array( [ [1.,2.,3.], [4.,5.,6.], [7.,8.,9.] ] )
  Y = np.array([1, 0])

  #X = csr_matrix(X)


  lp = LabelPropagation(gamma=1.0)
  lp.X_ = X

  affinity_matrix = lp._get_kernel(lp.X_)


  print ' x: \n', X

  affinity_matrix = X


  #affinity_matrix = csr_matrix(affinity_matrix)

  print 'type : ', type(affinity_matrix)
  print affinity_matrix

  normalizer = affinity_matrix.sum(axis=0)

  print ' normalizer : \n', normalizer

  if sparse.isspmatrix(affinity_matrix):
    print 'diag : \n', np.diag( np.array(normalizer) )
    affinity_matrix.data /= np.diag(np.array(normalizer))
  else:
    affinity_matrix /= normalizer[:, np.newaxis]

  if sparse.isspmatrix( affinity_matrix ):
    print affinity_matrix.todense()
  else:
    print affinity_matrix


if __name__ == "__main__":
  test_data()
  #test_data2()



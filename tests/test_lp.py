
import numpy as np
from graph_ssl.label_propagation import LabelPropagation
from graph_ssl.local_global_consistency import LocalGlobalConsistency
from graph_ssl.adsorption import Adsorption
from graph_ssl.modified_adsorption import ModifiedAdsorption
from graph_ssl.transduction_with_confidence import TransductionWithConfidence
from graph_ssl_dev.transduction_with_confidence_v0 import TransductionWithConfidenceV0
from graph_ssl.measure_propagation import MeasurePropagation
from graph_ssl.graph import Graph
from scipy.sparse import csr_matrix

#class Graph():
#  def __init__(self):
#    self.weight_matrix = np.array([[ 1.,  2.,  3.],
#                            [ 4.,  5.,  6.],
#                            [ 7.,  8.,  9.]])
#    self.weight_matrix = csr_matrix(self.weight_matrix)
#

def test_lp():
  lp = LabelPropagation( max_iter=50, alpha=0.9 )

  graph = Graph()
  graph.make_dummy_undirected_graph()
  #graph.make_dummy_directed_graph()
  #print 'weight_matrix csr :  \n' , graph.weight_matrix
  #print 'creat weight_matrix : \n', graph.weight_matrix.todense()

  lp.fit(graph)





def test_lgc():
  lgc = LocalGlobalConsistency( max_iter=50, alpha=0.5)
  graph = Graph()
  graph.make_dummy_undirected_graph()
  #graph.make_dummy_directed_graph()
  #print 'weight_matrix csr :  \n' , graph.weight_matrix
  print 'creat weight_matrix : \n', graph.weight_matrix.todense()

  lgc.fit(graph)


def test_ad():
  ad = Adsorption( max_iter=1, beta=2., tol=1e-3 )
  graph = Graph()

  graph.make_dummy_undirected_graph()
  #graph.make_dummy_directed_graph()
  #print 'weight_matrix csr :  \n' , graph.weight_matrix
  print 'creat weight_matrix : \n', graph.weight_matrix.todense()

  ad.fit(graph)




def test_mad():
  mad = ModifiedAdsorption( max_iter=50, beta=2., tol=1e-3 )
  graph = Graph()

  graph.make_dummy_undirected_graph()
  #graph.make_dummy_directed_graph()
  #print 'weight_matrix csr :  \n' , graph.weight_matrix
  print 'creat weight_matrix : \n', graph.weight_matrix.todense()

  mad.fit(graph)



def test_taco():
  taco = TransductionWithConfidence( max_iter=1, tol=1e-3 )
  graph = Graph()

  graph.make_dummy_undirected_graph()
  #graph.make_dummy_directed_graph()
  #print 'weight_matrix csr :  \n' , graph.weight_matrix
  print 'creat weight_matrix : \n', graph.weight_matrix.todense()

  Y = taco.fit(graph)
  print Y



def test_taco_v0():
  taco = TransductionWithConfidenceV0( max_iter=1, tol=1e-3 )
  graph = Graph()

  graph.make_dummy_undirected_graph()
  #graph.make_dummy_directed_graph()
  #print 'weight_matrix csr :  \n' , graph.weight_matrix
  print 'creat weight_matrix : \n', graph.weight_matrix.todense()

  Y = taco.fit(graph)
  print Y




def test_mp():
  mp = MeasurePropagation( max_iter=100, tol=1e-3, alpha=1.0, mu=1e-2, nu=1e-4 )
  graph = Graph()

  graph.make_dummy_undirected_graph()
  #graph.make_dummy_directed_graph()
  #print 'weight_matrix csr :  \n' , graph.weight_matrix
  print 'creat weight_matrix : \n', graph.weight_matrix.todense()

  mp.fit(graph)






import sys
import numpy as np
from graph_ssl.label_propagation import LabelPropagation
from graph_ssl.local_global_consistency import LocalGlobalConsistency
from graph_ssl.adsorption import Adsorption
from graph_ssl.modified_adsorption import ModifiedAdsorption
from graph_ssl.transduction_with_confidence import TransductionWithConfidence
from graph_ssl.measure_propagation import MeasurePropagation
from graph_ssl.graph import Graph
from scipy.sparse import csr_matrix
from scipy.io import mmwrite, mmread

from graph_ssl_dev.transduction_with_confidence_v0 import TransductionWithConfidenceV0
from graph_ssl_dev.local_transduction_with_confidence_v0 import LocalTransductionWithConfidenceV0
from graph_ssl_dev.local_transduction_with_confidence import LocalTransductionWithConfidence

class WebKbTest():
  def __init__(self):
    pass


  def load_graph(self):
    initfile = self.fileprefix + '-initlabel.mtx'
    Y0 = mmread(initfile)
    Y0 = Y0.todense()

    #print ' Y0 : ', type(Y0), ' \n', Y0

    wfile = self.fileprefix + '-weight.mtx'
    W = mmread(wfile)
    #print ' W : ', type(W), ' \n ', W

    graph = Graph()
    graph.n_classes = Y0.shape[1]
    graph.n_instances = Y0.shape[0]
    graph.init_labels = Y0
    graph.labeled = (Y0.sum(axis=1)>0).reshape(graph.n_instances, 1)

    self.num_seed = (Y0.sum(axis=1)>0).sum()
    print 'seed size : ', self.num_seed

    #print 'labeld : ', graph.labeled[:50, :]

    graph.weight_matrix = W.tocsr()

    self.graph = graph


    return graph


  def load_gold_label(self):
    labelfile = self.fileprefix + '-label.tag'
    y_gold = []
    tag_gold = []
    In = open(labelfile)
    for line in In:
      line =line.strip()
      if len(line) <=0:
        continue
      terms = line.split()
      if len(terms) !=2:
        print 'Error line : ', line
        continue
      tag_gold.append( terms[0].strip() )
      y_gold.append( int(terms[1]) )
    In.close()
    if len(y_gold) != self.graph.n_instances:
      print 'num_instances not match !!'
      print '   num_gold_labels : ', len(y_gold)
      print '   num_instances : ', self.graph.n_instances
    return y_gold


  def load_goldtag2inst(self):
    labelfile = self.fileprefix + '-label.tag'
    y_gold = []
    tag_gold = []
    tagId2inst = {}
    In = open(labelfile)
    i = 0
    for line in In:
      line =line.strip()
      if len(line) <=0:
        continue
      terms = line.split()
      if len(terms) !=2:
        print 'Error line : ', line
        continue
      tag_gold.append( terms[0].strip() )
      y_gold.append( int(terms[1]) )
      tag = terms[0].strip()
      tagId = int(terms[1].strip())
      if tagId not in tagId2inst:
        tagId2inst[tagId] = {i}
      else:
        tagId2inst[tagId].add(i)
      i += 1
    In.close()
    if i != self.graph.n_instances:
      print 'num_instances not match !!'
      print '   num_gold_labels : ', i
      print '   num_instances : ', self.graph.n_instances

    return tagId2inst




#######################3


  def get_acc(self, Y):
    y_pred = np.argmax(Y, axis=1)
    print '---- y_pred : ', len(y_pred), type(y_pred), y_pred.shape, y_pred.ndim, ' \n', y_pred
    if y_pred.ndim > 1:
      y_pred =[x for x in  y_pred.flat ]
      #print len(y_pred)
    y_gold = self.load_gold_label()
    #print '---- y_gold : ', len(y_gold), ' \n', y_gold
    y_test_pred = y_pred[self.num_seed: ]

    y_test_gold = y_gold[self.num_seed:]

    if len(y_test_pred) != len(y_test_gold):
      print 'test size not match !!'
      print '  y_test pred : ', len(y_test_pred)
      print '  y_test gold : ', len(y_test_gold)
      sys.exit()

    num = len(y_test_gold)
    correct = 0
    for i, t_g in enumerate(y_test_gold):
      t_p = y_test_pred[i]
      if t_g == t_p:
        correct += 1
    acc = float(correct)/float(num)
    print '  ACC : ', acc
    return acc


  def get_prbep(self, Y):
    #y_gold = self.load_gold_label()
    tagId2inst = self.load_goldtag2inst()
    #print tagId2inst

    print Y.shape
    (row, col) = Y.shape
    avg = 0
    for c in range(col):
      tdict = {}
      for i in range(row):
        tdict[i] = Y[i, c]
        #print ' i, c : ',i,c,  Y[i, c]
      n_correct = 0
      n_eval = 0
      n_gold = len(tagId2inst[c])
      for i, p in sorted(tdict.items(), key=lambda x:x[1], reverse=True):
        #print i, p
        n_eval += 1
        if i in tagId2inst[c]:
          #print ' i, ', i
          n_correct += 1
          #p = float(n_correct)/n_eval
          #r = float(n_correct)/n_gold
        if n_eval >= n_gold:
          p = float(n_correct)/n_eval
          #r = float(n_correct)/n_gold
          print ' class : ', c, ' size : ', n_gold,  ' prbep :  ', p
          avg += p
          break

    avg = float(avg)/len(tagId2inst.keys())

    print ' AVG PRBEP : ', avg


  def evaluate(self, Y):

    acc = self.get_acc(Y)

    self.get_prbep(Y)







  def get_learner(self):
    #lp = LabelPropagation( max_iter=100, alpha=1.0, debug=1 )
    #lp = LocalGlobalConsistency( max_iter=50, alpha=0.9, debug=1)
    #lp = Adsorption( max_iter=100, beta=2., tol=1e-3 )
    #lp = ModifiedAdsorption( max_iter=10, beta=5., tol=1e-3, debug=1 )
    #lp = TransductionWithConfidence( max_iter=10, alpha=1e0, beta=1e0, gamma=1.0, tol=1e-2, debug=0 )
    #lp = TransductionWithConfidenceV0( max_iter=10, alpha=1e0, beta=1e0, gamma=1.0, tol=1e-2, debug=0 )
    #lp = LocalTransductionWithConfidenceV0( max_iter=10, alpha=1e0, beta=1e0, gamma=1.0, tol=1e-2, debug=0 )
    #lp = LocalTransductionWithConfidence( max_iter=10, alpha=1e0, beta=1e0, gamma=1.0, tol=1e-2, debug=0 )
    lp = MeasurePropagation( max_iter=20, tol=1e-3, alpha=1e0, mu=1e0, nu=1e0 )



    return lp


  def main(self):
    print 'run WebKbTest main() '
    #self.fileprefix = '/uusoc/scratch/res/nlp/haibo/my-research/data/webkb-data/webkb-graph/sample-1'
    #self.fileprefix = '/scratch/hding/webkb-data/webkb-graph/sample-1'
    self.fileprefix = 'data/webkb-graph/sample-1'


    graph = self.load_graph()

    lp = self.get_learner()

    Y = lp.fit(graph)

    self.evaluate(Y)




def run_test_webkb():
  test = WebKbTest()
  test.main()




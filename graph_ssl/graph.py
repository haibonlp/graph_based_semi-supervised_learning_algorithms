import numpy as np
from scipy.sparse import csr_matrix

class Graph(object):
  def __init__(self):
    pass


  def loadGraph(self, infile):
    pass


  def get_labeled(self):
    return self.labeled


  def make_dummy_directed_graph(self):
    self.n_classes = 2
    self.n_instances = 4
    self.init_labels = np.array( [ [1., 0.], [0., 0.], [0.,0.], [0., 1.]] ).reshape(4,2)
    self.labeled = (self.init_labels.sum(axis=1) > 0 ).reshape(4, 1)

    print '----- labeled : \n', self.labeled

    self.weight_matrix = np.array([[ 0.,  1.,  2., 0.],
                                   [ 1.,  0.,  3., 0.],
                                   [ 2.,  3.,  0., 2.],
                                   [ 0.,  0.,  1., 0]])


    self.weight_matrix = csr_matrix(self.weight_matrix)




  def make_dummy_undirected_graph(self):
    self.n_classes = 2
    self.n_instances = 4
    #self.init_labels = np.array( [1., 0., 0., 0.] ).reshape(4,1)
    #self.init_labels = np.array( [1., 1., 1., 3.] ).reshape(4,1)
    #self.labeled = (self.init_labels > 0 ).reshape(4, 1)


    self.init_labels = np.array( [ [1., 0.], [0., 0.], [0.,0.], [0., 1.]] ).reshape(4,2)
    self.labeled = (self.init_labels.sum(axis=1) > 0 ).reshape(4, 1)

    print '----- labeled : \n', self.labeled



    self.weight_matrix = np.array([[ 0.,  1.,  2., 0.],
                                   [ 1.,  0.,  3., 0.],
                                   [ 2.,  3.,  0., 1.],
                                   [ 0.,  0.,  1., 0]])

    self.weight_matrix = csr_matrix(self.weight_matrix)







# coding=utf-8

import sys
import numpy as np
from sklearn.preprocessing import normalize
from scipy.sparse import csr_matrix
from scipy.sparse import diags
from scipy.stats import entropy

from utils.base import *
from base_adsorption import BaseAdsorption

class ModifiedAdsorption(BaseAdsorption):
  def __init__(self, debug=0,  max_iter=100, beta=2.0, tol=1e-3, mu1=1., mu2=1., mu3=1.):
    super(ModifiedAdsorption, self).__init__(max_iter, beta, tol)
    self.mu1 = mu1
    self.mu2 = mu2
    self.mu3 = mu3
    self.debug = debug



  def _cost(self, Y, Y0, S, L, R):

    v1 = self.mu1*np.trace( (Y0-Y).transpose().dot( S.dot(Y0-Y) ) )
    v2 = self.mu2*np.trace( Y.transpose().dot( L.dot(Y) ) )
    v3 = self.mu3*np.sum( np.power( Y-R, 2 ) )

    cost_value = v1 + v2 + v3


    return  cost_value



  def fit(self, graph):
    n_instances = graph.n_instances
    n_classes   = graph.n_classes

    Y_init = graph.init_labels
    Y0 = np.zeros((n_instances, n_classes+1))
    Y0[:, :-1] = Y_init
    Y = np.copy(Y0)
    r = np.zeros((n_instances, n_classes+1))
    r[:, -1] = 1


    # W must be csr_matrix matrix
    W = graph.weight_matrix

    check_weight_matrix(W)

    labeled = graph.get_labeled()

    self.compute_action_prob(W, labeled, isNormed=False)

    if self.p_inj[:, 0].ndim == 1:
      S_diag = np.diag(self.p_inj[:, 0])
    else:
      print 'S_diag error! '; sys.exit()
    #print ' S_diag : \n', S_diag


    #print '  \n W : \n', W.todense()
    #print '\n\n ]]]]]  -------------- \n\n'
    #a = np.array([1., 2., 3., 4.]).reshape(4,1); self.p_inj=a; self.p_cont=a; self.p_abnd=a
    #print ' p_inj : ', self.p_inj, ' -- \n', self.p_cont, ' --', self.p_abnd


    #print ' W shape : ', W.shape
    #print ' p_cont : ', self.p_cont.shape


    # W_hat = p_inj * W
    W_hat = W.multiply( csr_matrix(self.p_cont) )

    DD = np.array( W_hat.sum(axis=1) )
    DD_hat = np.array( W_hat.sum(axis=0) )
    #print 'DD : \n', DD[:, 0]
    #print 'DD_hat : \n', DD_hat[0, :]
    DD = diags([DD[:,0]], [0])
    DD_hat = diags([DD_hat[0,:]], [0])
    #print 'ddd : ', type(DD), ' \n', DD
    #print 'ddd_hat : ', type(DD_hat), '\n ', DD_hat
    LL =  DD + DD_hat - W_hat - W_hat.transpose()
    #print ' LL : ', type(LL), LL.shape, ' \n', LL
    RR = self.p_abnd * r
    #print ' RR : ', type(RR), RR.shape, ' \n', RR


    #print ' \n W_hat  : \n', W_hat.todense()
    #  L_vu = p_cont_v * W
    L_vu = W_hat + W_hat.transpose()

    ## Mvv should be n*(m+1) label matrix

    L_diag = L_vu.copy()
    #L_diag.setdiag(0)
    L_diag = L_diag.tolil(); L_diag.setdiag(0); # L_diag = L_diag.tocsr()
    #print 'L_diag : \n', L_diag.todense()
    #L_sum = np.sum(L_diag, axis=1)
    L_sum = L_diag.sum(axis=1)

    Mvv = self.mu1* self.p_inj + self.mu2* L_sum + self.mu3

    #print ' Mvv : \n', Mvv, ' type : ', type(Mvv)
    #print ' Lvu : \n', L_vu.todense(), '  ',type(L_vu)

    Y_pre = np.zeros((n_instances, n_classes+1))
    #print '---- Y0 : \n', Y

    pre_cost = float('inf')
    num_iter = 0
    while( num_iter < self.max_iter ):
      cur_cost = self._cost(Y, Y0, S_diag, LL, RR)
      diff_cost = pre_cost - cur_cost
      if diff_cost < self.tol :
        print 'ModifiedAdsorption converged at iter_num : ', num_iter
        break
      if self.debug > 0:
        print '##'*20, '\n\n   iter : ', num_iter,
        print ' cur_cost : ', cur_cost


      Y_pre = Y
      Dv = L_vu.dot(Y)
      #print '  ---- D : \n', Dv
      #print '  -- 1st : \n', self.mu1* self.p_inj*Y0
      #print ' ----2nd : \n', self.mu2* Dv
      #print ' ----3rd : \n', self.mu3 * self.p_abnd* r



      Y = self.mu1* self.p_inj * Y0 +  self.mu2* Dv + self.mu3*self.p_abnd* r
      #print ' raw Y : \n', Y
      Y /= Mvv
      #print '   Y/Mvv : \n', Y


      num_iter += 1

    normalize(Y, norm='l1', axis=1, copy=False)
    #print '  norm Y : \n', Y
    Y = Y[:, :-1]

    return Y















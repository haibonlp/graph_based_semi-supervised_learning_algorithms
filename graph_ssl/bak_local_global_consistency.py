# coding=utf-8

import sys
import numpy as np
from scipy import sparse
from scipy.sparse import csr_matrix
from sklearn.preprocessing import normalize
from sklearn.utils.graph import graph_laplacian
#from sklearn.utils.extmath import safe_sparse_dot


class LocalGlobalConsistency(object):
  def __init__(self, max_iter=100, alpha=1.0, tol=1e-3, debug=0):
    self.max_iter = max_iter
    self.alpha = alpha
    self.tol = tol
    self.debug = debug


  def _is_converged(self, Y, Y_hat):
    loss = np.sum( np.abs( Y - Y_hat ) )
    return loss < self.tol


  def _energy(self, Y, Y0,  nL):
    '''
    sigma_(i,j) w_ij * (Y_i/sqrt(D_ii) - Y_j/sqrt(D_jj) )^2
    = Y^T * nL * Y
    '''
    mu = float(1-self.alpha)/self.alpha
    v1 = mu * np.sum( np.power(Y-Y0, 2) )
    v2 = np.trace( Y.transpose().dot(nL.dot(Y) ) )


    energy_value = v1 + v2
    #print 'energy_value : ', energy_value
    return energy_value



  def fit(self, graph):
    n_instances = graph.n_instances
    n_classes   = graph.n_classes

    Y_init = graph.init_labels
    Y = np.copy(Y_init)

    # W must be csr_matrix matrix
    W = graph.weight_matrix


    #print ' energy_value :  ', self._energy(Y, laplacian)

    '''
    L = D - W
    nL = D^{-1/2} * L * D^{-1/2} = I - D^{-1/2}* W *D^{-1/2}
    New_W = D^{-1/2} * W * D^{-1/2} = I - nL
    '''

    # normalized laplacian
    #nL = graph_laplacian(W, normed=True)
    #W = sparse.identity( W.shape[0]) - nL
    D1 = W.sum(axis=0).A[0]
    D1[D1==0] += 1  # Avoid division by 0
    D2 = np.sqrt(sparse.diags((1.0/D1),offsets=0))
    W = D2.dot(W).dot(D2)




    W = W.tocsr()
    #print ' normalized  weight_matrix : \n',  W.todense()

    #sys.exit()

    Y_pre = np.zeros( Y_init.shape )
    num_iter = 0
    pre_energy = float('inf')
    while( num_iter < self.max_iter ):
      #cur_energy = self._energy(Y, Y_init, nL)
      diff_energy = pre_energy - cur_energy
      if diff_energy < self.tol:
        print 'LGC converged at iter_num : ', num_iter
        break
      pre_energy = cur_energy

      if self.debug > 0:
        print '##'*20, '\n\n   iter : ', num_iter,
        print ' cur_energy : ', cur_energy
      Y_pre = Y
      if self.debug >1:
        print '\b Y : ', Y
      Y =  self.alpha * W.dot(Y) + (1-self.alpha) * Y_init
      if self.debug >1:
        print 'dot : \n', Y

      num_iter += 1

      #print ' energy_value :  ', self._energy(Y, laplacian)

    ###Y_normalizer = np.sum(Y, axis=1).reshape( n_instances, 1)
    ###print Y_normalizer
    ###Y /= Y_normalizer

    normalize(Y, norm='l1', axis=1, copy=False)


    if self.debug > 1:
      print ' normalized Y : \n', Y, type(Y)
    return Y










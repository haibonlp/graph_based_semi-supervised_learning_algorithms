# coding=utf-8

import sys
import numpy as np
from sklearn.preprocessing import normalize
from scipy.sparse import csr_matrix
from scipy.stats import entropy

from utils.base import *
from base_adsorption import BaseAdsorption

class Adsorption(BaseAdsorption):

  def fit(self, graph):
    n_instances = graph.n_instances
    n_classes   = graph.n_classes

    Y_init = graph.init_labels
    ##Y0 = np.append(Y_init, np.zeros((n_instances,1)), axis=1)  # slow
    Y0 = np.zeros((n_instances, n_classes+1))
    Y0[:, :-1] = Y_init
    Y = np.copy(Y0)
    r = np.zeros((n_instances, n_classes+1))
    r[:, -1] = 1

    #Y = np.copy(Y_init)

    # W must be csr_matrix matrix
    W = graph.weight_matrix

    check_weight_matrix(W)


    #W /= W.sum(axis=1)
    normalize(W, norm='l1', axis=1, copy=False)
    print ' normalized  weight_matrix : \n',  W.todense()
    labeled = graph.get_labeled()

    self.compute_action_prob(W, labeled, True)



    Y_pre = np.zeros((n_instances, n_classes+1))
    print '---- Y0 : \n', Y

    num_iter = 0
    while( num_iter < self.max_iter ):
      if self._is_converged(Y_pre, Y):
        print 'Adsorption converged at iter_num : ', num_iter
        break
      print '##'*20, '\n\n   iter : ', num_iter
      Y_pre = Y
      #Y = self.p_inj*Y0 + self.p_cont*W.dot(Y) + self.p_abnd* r
      D = W.dot(Y)

      print '---- D : \n', D, type(D)
      print '  -- 1st : \n', self.p_inj*Y0
      print ' ----2nd : \n', self.p_cont*D
      print ' ----3rd : \n', self.p_abnd* r

      Y = self.p_inj*Y0 + self.p_cont*D + self.p_abnd* r

      normalize(Y, norm='l1', axis=1, copy=False)
      print '---- Y : \n', Y
      num_iter += 1
    print 'Y_shape : ',  Y.shape, type(Y)
    print  Y[0, :]
    Y = Y[:, :-1]

    print 'Y_shape[:, :-1] : ',  Y.shape, type(Y)
    print  Y

    return Y










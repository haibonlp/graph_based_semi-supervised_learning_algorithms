# coding=utf-8

import sys
import numpy as np
from sklearn.preprocessing import normalize
from scipy.sparse import csr_matrix
from scipy.stats import entropy

from utils.base import *

class BaseAdsorption(object):
  def __init__(self, max_iter=100, beta=2.0,  tol=1e-3):
    self.max_iter = max_iter
    self.tol = tol
    self.beta = beta

  def _is_converged(self, Y, Y_hat):
    loss = np.sum( np.abs( Y - Y_hat ) )
    return loss < self.tol


  def compute_action_prob(self, W, labeled, isNormed=False ):
    ''' the probability for each action: p_inj, p_cont, p_abnd
    Parameters
    -----------
      W : the weight_matrix
    '''
    if not isNormed:
      W = normalize(W, norm='l1', axis=1, copy=True)
    #print ' prob matrix : \n', W.todense()

    n_row = W.shape[0]
    H = np.zeros((n_row, 1))
    for i in xrange(n_row):
      a = W[i, :]
      h = entropy( np.array( a[ a.nonzero()] ).reshape(-1) )
      H[i, 0] = h
    #print 'entropy : \n', H
    Cv = np.log(self.beta) / np.log( self.beta + np.exp(H) )

    #print ' ---- Cv : \n', Cv
    SeedIdentity = np.zeros((n_row, 1))
    SeedIdentity[labeled] = 1.0
    #print 'SeedIdentity : \n', SeedIdentity
    Jv = (1.0 - Cv) * np.sqrt( H ) * SeedIdentity
    #print ' ---- Jv : \n', Jv
    Zv = np.maximum(Cv+Jv, 1.0)

    #print ' ---- Zv : \n', Zv

    self.p_inj  = Jv/Zv
    self.p_cont  = Cv/Zv
    self.p_abnd = 1.0 - self.p_inj - self.p_cont

    #print ' -- p_inj \n ', self.p_inj
    #print ' -- p_cont \n ', self.p_cont
    #print ' -- p_abnd \n ', self.p_abnd



    #sys.exit()






##  def fit(self, graph):
##    n_instances = graph.n_instances
##    n_classes   = graph.n_classes
##
##    Y_init = graph.init_labels
##    ##Y0 = np.append(Y_init, np.zeros((n_instances,1)), axis=1)  # slow
##    Y0 = np.zeros((n_instances, n_classes+1))
##    Y0[:, :-1] = Y_init
##    Y = np.copy(Y0)
##    r = np.zeros((n_instances, n_classes+1))
##    r[:, -1] = 1
##
##    #Y = np.copy(Y_init)
##
##    # W must be csr_matrix matrix
##    W = graph.weight_matrix
##
##    check_weight_matrix(W)
##
##
##    #W /= W.sum(axis=1)
##    normalize(W, norm='l1', axis=1, copy=False)
##    print ' normalized  weight_matrix : \n',  W.todense()
##    labeled = graph.get_labeled()
##
##    self.compute_action_prob(W, labeled, True)
##
##
##
##    Y_pre = np.zeros((n_instances, n_classes+1))
##    print '---- Y0 : \n', Y
##
##    num_iter = 0
##    while( num_iter < self.max_iter ):
##      if self._is_converged(Y_pre, Y):
##        print 'Program converged at iter_num : ', num_iter
##        break
##      print '##'*20, '\n\n   iter : ', num_iter
##      Y_pre = Y
##      #Y = self.p_inj*Y0 + self.p_cont*W.dot(Y) + self.p_abnd* r
##      D = W.dot(Y)
##      print '---- D : \n', D
##      print '  -- 1st : \n', self.p_inj*Y0
##      print ' ----2nd : \n', self.p_cont*D
##      print ' ----3rd : \n', self.p_abnd* r
##
##      Y = self.p_inj*Y0 + self.p_cont*D + self.p_abnd* r
##
##      normalize(Y, norm='l1', axis=1, copy=False)
##      print '---- Y : \n', Y
##      num_iter += 1
##










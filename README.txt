
This tool is implemented by Haibo Ding, Email: dinghaibo.cn@gmail.com


Implemented several graph based semi-supervised learning methods which includes: 

(1) basic label propagation, 
(2) Adsorption, 
(3) Modified Adsorption, 
(4) Local and global consistency algorithm, 
(5) measure propagation
(6) transduction with confidence algorithm




#import numpy as np
from scipy.sparse import csr_matrix

def check_weight_matrix(w):
  if not isinstance(w, csr_matrix):
    raise ValueError(" Error! The weight matrix is not in sparse csr format")



